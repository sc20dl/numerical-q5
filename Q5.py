""" runNumerical.py

Script that compares euler and midpoint numerical methods

To run via commandline use one of the folllowing:

python runNumerical.py -m euler python runNumerical.py -m midpoint

"""

# Python modules
import getopt
import sys
import matplotlib.pyplot as plt
import numpy as np

def make_heat_matrix(N):
    # initialise matrix
    A = np.zeros((N, N))
    h = 1.0 / (N - 1)

    # fill entries
    for i in range(1, N - 1):
        A[i][i - 1] = 1.0 / h ** 2
        A[i][i] = -2.0 / h ** 2
        A[i][i + 1] = 1.0 / h ** 2

    return A

def rhs(t, y):
    """
    Return a value for the right-hand side of the differential equation
    y'(t) = f(t,y)

    ARGUMENTS:  t   the current value of t
                y   the current value of y

    RETURNS:    f   the value of f(t,y)
    """

    return ( ((t * y[0])/h) - (2*(y[1] * t)/h) + ((t * y[2])/h) )

def implicitEuler(N, dt, x0, T):
    """
    Implement the implicit Euler method for our heat problem. 
    Your function should take arguments:
        N, the size of the system; 
        
        dt, the step size;
        
        x0, the initial value
        
        T, the final time
        
        return the value of x at the final time step\vec{x}^{(0)} 

    """
    n = int(T/dt) # gets the number of time steps
    I = np.identity(N)
    A = make_heat_matrix(N) # Make the heat matrix for the system
    u = np.zeros((N,1))
    print(u)
    #print(x0)
    B = (I - dt *A)
    
    temp = gauss_seidel(B,x0,x0,n)
                
def test():
    t = np.array([[20.0],[20.0],[20.0],[20.0],[20.0],[20.0],[20.0],[20.0],[20.0],[80.0]])
    #t = np.array([20,20,20,20,20,20,20,20,20,80])
    implicitEuler(10,1,t,10)
    


def gauss_seidel(A, u, b, n_iterations):
    """
    Solve the system A u = b using a Gauss-Seidel iteration

    ARGUMENTS:  A   k x k matrix
                u   k-vector storing initial estimate
                b   k-vector storing right-hand side
                n_iterations
                    integer number of iterations to carry out

    RESULTS:    u   k-vector storing solution
    """

    # Get dimension
    k = len(A)

    # Make sure matrix A is float
    A = A.astype(float)

    for i in range(n_iterations):
        for j in range(k):
            u[j] = u[j] + (b[j] - np.dot(A[j, :], u)) / A[j, j]
        print(u)

    return u


def runEuler():
    """
    Function that calls the Euler function with smaller and smaller values of
    dt and plots the results.
    """

    # set up figure
    plt.figure()
    # print table header
    print("n    dt      soln    error")
    print("---  ------  ------  ------")
    # ensure t is defined
    t = np.array([1.0, 2.0])
    temp = np.array([20,20,20,20,20,20,20,20,20,80])
    
    # Set the number of time steps to be taken
    for n in [10]:
        # run euler scheme
        t, y = eulerN(rhs, 0.0, temp, 10.0, n)
        #plt.plot(t, y, label=f"n = {n}")

        # compute error and time step
        dt = (2.0 - 1.0) / float(n)
        soln = y[n][0]
        error = abs(soln - 8.0)

        # print output
        print(f"{n:3d} {dt:7.4f} {soln:7.4f} {error:7.4f}")
        print(y[n,:])

    # plot results
    np.set_printoptions(precision=3, suppress=True)
    plt.legend(loc="upper left")
    plt.xlim([t.min(), t.max()])
    plt.ylim([0, 8])
    plt.savefig("euler.svg")


def eulerN(rhs, t0, y0, tfinal, n):
    """
    Use Euler's method to solve N number of differential equation y'(t)=f(t,y) subject
    to the initial condition y(t0) = y0.

    ARGUMENTS:  t0  initial value of t
                y0  N-dimensional array of initial value of y(t) when t=t0
                tfinal final value of t for which the solution is required
                n   the number of sub-intervals to use for the approximation
                rhs function of right-hand side of differential equation


    RESULTS:    t   (n+1)-vector storing the values of t at which the solution
                    is estimated
                y   N x (n+1)-matrix array storing the estimated solution.
    """

    # Get dimensions
    N = len(y0)
    
    #Initialise matrix A
    # Initialise the transposed matrix
    # Multiply transposed matrix by dt each time
    # Multiply tm by A

    # Initialise the arrays ta and y
    t = np.zeros([n + 1, 1])
    y = np.zeros([n + 1, N])  # N x (n+1) matrix
    t[0] = t0 # this stores the inital value of t, which is 0
    y[0, :] = y0
    A = make_heat_matrix(N)
    #y[i,: ]gets each row from the 2D matrix
    # so we can use RHS to do matrix multiplication and store the total in
    # a variable to return back

    # Calculate the size of each interval
    #dt = (tfinal - t0) / float(n)
    dt = 2**(-8)
    # Take n steps of Euler's method
    for i in range(n):
        print(y)
        # y[i, :] stores each row in the matrix
        #t[i] is currently zero? so that is why it isn't working
        y[i + 1, :] = y[i, :] + dt * (A @ y[i, :])
        t[i + 1] = t[i] + dt

    return t, y

